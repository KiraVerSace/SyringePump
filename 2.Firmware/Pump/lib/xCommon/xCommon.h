/*
 * @Description  : Common definitions include pin assigns
 * @Version      : 0.1
 * @Company      : V-Think Development Team
 * @Author       : KiraVerSace@yeah.net
 * @Date         : 2020-07-08 16:55:12
 * @LastEditTime : 2022-03-29 18:14:12
 */
#ifndef __X_COMMON_H__
#define __X_COMMON_H__

#include <xCode.h>
#include <Arduino.h>
#include <STM32FreeRTOS.h>
#include <STM32TimerInterrupt.h>

/*
 * 不会因为除了掉电以外的任何复位而清零
 * 可以用来统计软复位的次数
 */
extern uint32_t rebootCount;

extern void xCommonInit(void);

#include <stm32yyxx_ll_adc.h>
/* 必须初始化里面加上 analogReadResolution(12) */
extern uint32_t readVref(void);			// 读取 STM32 内部校正过后的参考电压
extern uint32_t readTempSensor(void);	// 读取 STM32 内部温度传感器
extern uint32_t readButtonCell(void);	// 读取 STM32 后备电池电压


/*----------------- Pin Assign -----------------*/
/*Periph Module*/
const uint32_t LED    = LED_BUILTIN;
const uint32_t BUZZER = PA15;
const uint32_t BUTTON = PC8;

/*----------------- xShell/xLog  -------------------*/
#include <cpp_support/shell_cpp.h>  // 此Shell调用了LetterShell实现了CLI与LOG
#include <log/log.h>
extern void xShellRunTask(void);

/*----------------- xButton -------------------*/
#include <OneButton.h>
extern OneButton xButton;

#endif
