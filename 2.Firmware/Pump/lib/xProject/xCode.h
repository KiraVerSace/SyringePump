/*
 * @Description  : 用于存放项目以及作者的一些信息
 * @Version      : 0.1
 * @Company      : V-Think Development Team
 * @Author       : KiraVerSace@yeah.net
 * @Date         : 2022-03-25 17:33:31
 * @LastEditTime : 2022-03-26 20:37:10
 */
#ifndef __X_CODE_H__
#define __X_CODE_H__

#define	DEVELOPER			"KiraVerSace@yeah.net"
#define HARDWARE_VERSION	"V/0.1"
/*
 * V0.0.1 [2022-03-25] 初始版本
 * V0.1.0 [2021-10-18]
 */
#define SOFTWARE_VERSION	"V/0.1.1"

#endif
