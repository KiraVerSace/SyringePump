<!--
 * @Description  :
 * @Version      : 0.1
 * @Company      : V-Think Development Team
 * @Author       : KiraVerSace@yeah.net
 * @Date         : 2022-03-27 02:06:05
 * @LastEditTime : 2022-03-29 00:54:15
-->
# General PlatformIO Framework
![version](https://img.shields.io/badge/version-0.0.1-brightgreen.svg)
![build](https://img.shields.io/badge/build-2022.03.27-brightgreen.svg)

![VThink-Full.jpg](https://i.imgtg.com/2022/03/27/bLunF.png)

Based on PlatformIO and with STM32[`STM32L476RGT6`]

Create a general framwork for all the projects.

Library:

1.[STM32duino FreeRTOS](https://github.com/stm32duino/STM32FreeRTOS)

2.[letterShell](https://github.com/NevermindZZT/letter-shell)

3.[OneButton](https://github.com/mathertel/OneButton)

4.[STM32_TimerInterrupt](https://github.com/khoih-prog/STM32_TimerInterrupt)
