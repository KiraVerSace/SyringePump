/*
 * @Description  : main.cpp
 * @Version      : 0.1
 * @Company      : V-Think Development Team
 * @Author       : KiraVerSace@yeah.net
 * @Date         : 2022-03-22 19:20:43
 * @LastEditTime : 2022-03-31 13:46:32
 */
#include <xCommon.h>

/*
 * 看门狗
 */
#define IWDG_INTERVAL 				10
void iWatchdogTask(void *pvParameters);
static TaskHandle_t iWatchdogTaskHandle = NULL;

/*
 * 按键,Shell,等需要及时处理的函数
 */
#define PERIPH_INTERVAL 			10
void periphRunTask(void *pvParameters);
static TaskHandle_t periphTaskHandle = NULL;

void setup()
{
	xCommonInit();

	xTaskCreate(iWatchdogTask, 		(const portCHAR *)"iWDG", 			256, 	NULL, 	2,	&iWatchdogTaskHandle);
	xTaskCreate(periphRunTask, 		(const portCHAR *)"periphRun", 		512, 	NULL, 	1,	&periphTaskHandle);

	pinMode(LED_BUILTIN, OUTPUT);

	vTaskStartScheduler();
  	while(1);
}

void loop()
{

}

void iWatchdogTask(void *pvParameters)
{
	logDebug("iWatchdogTask Start...[%d]", uxTaskGetStackHighWaterMark(NULL));

	logDebug("Reboot count = %d", rebootCount);
	for (;;)
	{
		digitalWrite(LED_BUILTIN, HIGH);
		vTaskDelay(100);
		digitalWrite(LED_BUILTIN, LOW);
		vTaskDelay(100);
	}
}

void periphRunTask(void *pvParameters)
{
	logDebug("periphRunTask Start...[%d]", uxTaskGetStackHighWaterMark(NULL));

	for (;;)
	{
		xShellRunTask();	// xShell Loop
		vTaskDelay(1);
	}
}
