/*
 * @Description  : Common definitions include pin assigns
 * @Version      : 0.1
 * @Company      : V-Think Development Team
 * @Author       : KiraVerSace@yeah.net
 * @Date         : 2020-07-08 16:55:12
 * @LastEditTime : 2021-01-14 17:07:21
 */
#ifndef __X_COMMON_H__
#define __X_COMMON_H__

#include <Arduino.h>
#include <STM32FreeRTOS.h>


/* Pin assign */
/*-------   Bluetooth   -------*/
#define XSHELL_UART 	Serial

/*------- Periph Module -------*/
const 	int32_t 	LED   		= LED_GREEN;
const 	int32_t 	S_BUTTON	= USER_BTN;
/*-------   Step Motor  -------*/
const int32_t Z_CS_PIN		= PB6;
const int32_t Z_SCK_PIN 	= PA5;
const int32_t Z_MOSI_PIN	= PA7;
const int32_t Z_MISO_PIN	= PA6;

/*------------------------------------------------------------*/
extern void xSystemInit(void);

#include <OneButton.h>
extern OneButton sButton;

#include <stm32yyxx_ll_adc.h>
extern uint32_t readVref(void);			// 读取 STM32 内部校正过后的参考电压
extern uint32_t readTempSensor(void);	// 读取 STM32 内部温度传感器
extern uint32_t readButtonCell(void);	// 读取 STM32 后备电池电压

#include <cpp_support/shell_cpp.h>		// 此Shell调用了LetterShell实现了CLI与LOG
#include <log/log.h>
extern void xShellRun(void);

#include <Estee_TMC5130.h>
const float	  Z_R_SENSE		= 0.075F;
extern Estee_TMC5130 ZStepper;

#endif
