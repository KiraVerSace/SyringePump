/*
 * @Description  :
 * @Version      : 0.1
 * @Company      : V-Think Development Team
 * @Author       : KiraVerSace@yeah.net
 * @Date         : 2021-01-14 17:10:32
 * @LastEditTime : 2021-01-14 17:10:32
 */
#ifndef __X_STEP_MOTOR_H__
#define __X_STEP_MOTOR_H__

#include <XCommon.h>

class XStepMotor
{
private:
	/* data */
public:
	XStepMotor(/* args */);
	~XStepMotor();
};

#endif
