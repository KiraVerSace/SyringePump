/*
 * @Description  :
 * @Version      : 0.1
 * @Company      : V-Think Development Team
 * @Author       : KiraVerSace@yeah.net
 * @Date         : 2020-10-17 04:31:06
 * @LastEditTime : 2020-12-29 01:03:21
 */
#ifndef __X_INTERFACE_COMMAND_H__
#define __X_INTERFACE_COMMAND_H__

#include <shell.h>
#include <log/log.h>
#include <STM32FreeRTOS.h>

#define XSHELL_UART Serial

extern Log xLog;
extern void xShellCommandInit(void);
extern void xShellRun(void);


#endif
