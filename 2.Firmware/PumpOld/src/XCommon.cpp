/*
 * @Description  : Common definitions include pin assigns
 * @Version      : 0.1
 * @Company      : V-Think Development Team
 * @Author       : KiraVerSace@yeah.net
 * @Date         : 2020-07-09 14:44:50
 * @LastEditTime : 2021-03-03 00:19:36
 */
#include <XCommon.h>

OneButton sButton(S_BUTTON, true, true);
static void sButtonClick(void);
static void sButtonDoubleClick(void);
static void sButtonLongPressStart(void);
static void sButtonLongPressStop(void);
static void sButtonLongPress(void);

Estee_TMC5130 ZStepper(Z_CS_PIN);

static void xShellInit(void);
static void xLogInit(void);

static void systemHardwareInit(void)
{
	/* Set Interrupt Group Priority */
	HAL_NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);

    /* 系统 Seria 初始化，并且清空串口缓存 */
	Serial.begin(115200);
	while (Serial.read() >= 0) { }
	/* XShell 初始化，指定某个串口交互 */
	xShellInit();
	xLogInit();
	logInfo("System Hardware Init...");

	/* IO 端口初始化 */
	pinMode(LED, OUTPUT);
	pinMode(S_BUTTON, INPUT);
	digitalWrite(LED, HIGH);
}

static void systemSoftwareInit(void)
{
	/* OneButton 按键接口回调函数初始化 */
	sButton.attachClick(sButtonClick);
	sButton.attachDoubleClick(sButtonDoubleClick);
	sButton.attachDuringLongPress(sButtonLongPress);
	sButton.attachLongPressStart(sButtonLongPressStart);
	sButton.attachLongPressStop(sButtonLongPressStop);
}

void xSystemInit(void)
{
	systemHardwareInit();	// 必须先于软件初始化
	delay(10);				// 延时等待硬件初始化稳定
	systemSoftwareInit();
}

uint32_t readVref(void)
{
	return (__LL_ADC_CALC_VREFANALOG_VOLTAGE(analogRead(AVREF), LL_ADC_RESOLUTION_12B));
}

uint32_t readTempSensor(void)
{
	int32_t vRef = __LL_ADC_CALC_VREFANALOG_VOLTAGE(analogRead(AVREF), LL_ADC_RESOLUTION_12B);
	return (__LL_ADC_CALC_TEMPERATURE(vRef, analogRead(ATEMP), LL_ADC_RESOLUTION_12B));
}

uint32_t readButtonCell(void)
{
	int32_t vRef = __LL_ADC_CALC_VREFANALOG_VOLTAGE(analogRead(AVREF), LL_ADC_RESOLUTION_12B);
	/*The VBAT pin is internally connected to a bridge divider by 3*/
	return (__LL_ADC_CALC_DATA_TO_VOLTAGE(vRef, analogRead(AVBAT), LL_ADC_RESOLUTION_12B) * 3);
}

static void sButtonClick(void)
{
	logDebug("sButton click.");
}

static void sButtonDoubleClick(void)
{
	logDebug("sButton doubleclick.");
}

static void sButtonLongPressStart(void)
{
	logDebug("sButton longPress start.");
}

static void sButtonLongPressStop(void)
{
	logDebug("sButton longPress stop.");
}

static void sButtonLongPress(void)
{
	logDebug("sButton longPress...");
}

/*-------------- SHELL -----------------*/
Shell xShell;

static HardwareSerial *xShellSerial = &XSHELL_UART;
static char xShellBuffer[1024];
static void xShellWrite(const char ch)
{
	if (xShellSerial->availableForWrite())
	{
		xShellSerial->write(ch);
	}
}

static int8_t xShellRead(char *ch)
{
	if(xShellSerial->available())
	{
		*ch = xShellSerial->read();

		return 0;
	}
	else
	{
		return -1;
	}
}

static void xShellInit(void)
{
	xShell.write = xShellWrite;
	xShell.read  = xShellRead;

	shellInit(&xShell, xShellBuffer, sizeof(xShellBuffer));
}

void xShellRun(void)
{
	shellTask(&xShell);
}

/*-------------- LOG -----------------*/
Log xLog;
static void xLogWrite(char *buffer, short size)
{
	if (xShellSerial->availableForWrite())
	{
		xShellSerial->write(buffer, size);
	}
}

static void xLogInit(void)
{
	xLog.write = xLogWrite;
	xLog.active = true;
	xLog.level = LOG_DEBUG;

	logRegister(&xLog, NULL);
}

/* 支持尾行模式的Log初始化函数 */
// static void xLogWrite(char *buffer, short size)
// {
// 	if (xLog.shell)
// 	{
// 		shellWriteEndLine(xLog.shell, buffer, size);
// 	}
// }

// static void xLogInit(void)
// {
// 	xLog.write = xLogWrite;
// 	xLog.active = true;
// 	xLog.level = LOG_DEBUG;

// 	logRegister(&xLog, &xShell);
// }

