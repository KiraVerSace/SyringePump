/*
 * @Description  :
 * @Version      : 0.1
 * @Company      : V-Think Development Team
 * @Author       : KiraVerSace@yeah.net
 * @Date         : 2020-12-24 16:21:43
 * @LastEditTime : 2021-01-14 17:14:18
 */
#include <XCommon.h>


#define PERIPH_INTERVAL 			100
void periphRunTask(void *pvParameters);

#define MOTOR_CONTROL_INTERVAL 		5000
void motorControlTask(void *pvParameters);

void setup()
{
	xSystemInit();
	// pinMode(Z_CS_PIN, OUTPUT);
	// digitalWrite(Z_CS_PIN, HIGH);

	// ZStepper.begin(0x4, 0x4, NORMAL_MOTOR_DIRECTION);

	// ZStepper.writeRegister(VSTART, 0x0);
	// ZStepper.writeRegister(A_1, 1000);
	// ZStepper.writeRegister(V_1, 50000);
	// ZStepper.writeRegister(AMAX, 500);
	// ZStepper.writeRegister(VMAX, 200000);
	// ZStepper.writeRegister(DMAX, 700);
	// ZStepper.writeRegister(D_1, 1400);
	// ZStepper.writeRegister(VSTOP, 10);
	// ZStepper.writeRegister(TZEROWAIT, 0);
	// ZStepper.writeRegister(RAMPMODE, 0);
	// ZStepper.writeRegister(XTARGET, 0);

	xTaskCreate(periphRunTask, 		(const portCHAR *)"PeriphRun", 	512, 	NULL, 	6,	NULL);
	xTaskCreate(motorControlTask, 	(const portCHAR *)"Motor", 		512, 	NULL, 	5, 	NULL);

	vTaskStartScheduler();
 	logError("Insufficient RAM");
  	while(1);
}

void loop(void)
{

}

void periphRunTask(void *pvParameters)
{
	Serial.println("\r\nperiphRunTask");

	for (;;)
	{
		xShellRun();
		sButton.tick();

		digitalToggle(LED_BUILTIN);
		vTaskDelay(PERIPH_INTERVAL);
	}
}


void motorControlTask(void *pvParameters)
{
	Serial.println("\r\nmotorControlTask");

	for (;;)
	{

		vTaskDelay(MOTOR_CONTROL_INTERVAL);
	}
}
