/*
 * @Description  :
 * @Version      : 0.1
 * @Company      : V-Think Development Team
 * @Author       : KiraVerSace@yeah.net
 * @Date         : 2021-01-07 10:26:25
 * @LastEditTime : 2021-01-13 18:11:31
 */
#include <XCommon.h>


static void freertosTaskList(void)
{
	char taskListBuffer[256];
	/*
	 *'X' - Running
	 *'B' – Blocked
	 *'R' – Ready
	 *'D' – Deleted (waiting clean up)
	 *'S' – Suspended, or Blocked without a timeout
	 */
	vTaskList((char *)&taskListBuffer);
	Shell *shell = shellGetCurrent();
    if (shell)
    {
        shellPrint(shell, "TaskName      State  Priority FreeStack NO\r\n");
		shellPrint(shell, taskListBuffer);
    }
}
SHELL_EXPORT_CMD(
SHELL_CMD_PERMISSION(0)|SHELL_CMD_TYPE(SHELL_TYPE_CMD_FUNC)|SHELL_CMD_DISABLE_RETURN,
TL, freertosTaskList, List freeRTOS Task Status);

